#ifndef _Tijdsduur_
#define _Tijdsduur_
#include <iostream>
using namespace std;

class Tijdsduur {
public:

	Tijdsduur(int u, int m) : min(m + (u * 60)) {
	}
	Tijdsduur(int m) : min(m) {
	}
	void eraf(Tijdsduur t);
	void print();
	Tijdsduur erbij(Tijdsduur t);
	Tijdsduur erbij(int y);
	void maal(int x);
	void print(ostream& o) const;
	Tijdsduur& operator += (const Tijdsduur& right);
private:
	int min = 0;
};



#endif // !_Tijdsduur_
#pragma once
