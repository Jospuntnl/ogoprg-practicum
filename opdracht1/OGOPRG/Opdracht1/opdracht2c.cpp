﻿#include <iostream>           // nodig voor cout (schrijven naar scherm)
#include <iomanip>            // nodig voor setw (veldbreedte defini�ren)
using namespace std;

// De declaratie van de ADT Tijdsduur:

class Tijdsduur {
public:

	Tijdsduur(int u, int m) : min(m), uren(u) {
		normaliseer();
	}
	Tijdsduur(int m) : min(m) {
		normaliseer();
	}
	void eraf(Tijdsduur t);
	void print() ;
	Tijdsduur erbij(Tijdsduur t);
	Tijdsduur erbij(int y);
	void maal(int x);
	void print(ostream& o) const;
	void nieuw(int u, int m);
	Tijdsduur& operator += (const Tijdsduur& right);
private:
	int uren = 0;
	int min = 0;
	void normaliseer();
};

ostream& operator<<(ostream& left, const Tijdsduur& right);

void Tijdsduur::nieuw(int u, int m) {
	uren = u;
	min = m;
}

void Tijdsduur::eraf(Tijdsduur t) {
	min -= t.min;
	normaliseer();
}

Tijdsduur Tijdsduur::erbij(Tijdsduur t) {
	min += t.min;
	normaliseer();
	return uren, min;
}

Tijdsduur Tijdsduur::erbij(int y) {
	min = min + y;
	return min;
}

void Tijdsduur::maal(int x) {
	min = (uren * 60) + min;
	min = min * x;
	normaliseer();
}

void Tijdsduur::normaliseer() {
	if (min >= 60) {
		uren = uren + (min / 60);
		min = (min % 60);
	}
	if (min < 0) {
		uren = uren - 1;
		min = 60 + min;
		normaliseer();
	}
	if (uren < 0) {
		uren = 0;
		min = 0;
	}
}

void Tijdsduur::print()  {
	normaliseer();
	if (uren == 0) {
		cout << min << " minuten.";
	}
	else if ((uren == 1) && (min == 0)) {
		cout << uren << " uur.";
	}
	else if ((uren == 1) && (min >> 0)) {
		cout << uren << " uur en " << min << " minuten.";
	}
	else if ((uren >> 0) && (min == 0)) {
		cout << uren << " uren.";
	}
	else {
		cout << uren << " uren en " << min << " minuten.";
	}
}

void Tijdsduur::print(ostream& o) const {
    // Deze functie moet de Tijdsduur wegschrijven naar de uitvoer genaamd "o"
	if (uren == 0) {
		o << min << " minuten.";
	}
	else if ((uren == 1) && (min == 0)) {
		o << uren << " uur.";
	}
	else if ((uren == 1) && (min >> 0)) {
		o << uren << " uur en " << min << " minuten.";
	}
	else if ((uren >> 0) && (min == 0)) {
		o << uren << " uren.";
	}
	else {
		o << uren << " uren en " << min << " minuten.";
	}
}

// Deze operator hoef je nu nog niet te begrijpen
// In les 7 wordt dit uitgelegd	
ostream& operator<<(ostream& left, const Tijdsduur& right) {
    right.print(left);
    return left;
}

Tijdsduur& Tijdsduur::operator+=(const Tijdsduur& right) {
	this->erbij(right);
	return *this;
}
Tijdsduur operator - (Tijdsduur left, Tijdsduur right) {
	left.eraf(right);
	return left;
}

int main5() {
	int i = 0;
	int u = 0;
	int m = 0;
	int min = 0;
	Tijdsduur t4(0, 0);
	Tijdsduur t3(0, 0);
	cout << "Start" << endl;
    Tijdsduur t1(3, 50);        // t1 is 3 uur en 50 minuten
    cout << "t1 = " << t1 << endl;
    const Tijdsduur kw(15);     // kw is 15 minuten
    cout << "kw = " << kw << endl;
    t1 += kw;                   // Tel kw bij t1 op
    cout << "t1 = " << t1 << endl;
    Tijdsduur t2(t1 - kw);      // t2 is t1 min kw
    cout << "t2 = " << t2 << endl;
	for (i = 9; i > -1; i--) {
		cout << "Geef het aantal uren: ";
		cin >> u;
		cout << u << endl;
		cout << "Geeft het aantal minuten: ";
		cin >> m;
		cout << m << endl;
		min = (60 * u) + m;
		t4.erbij(min);
		cout << "Nog " << i << " keer!" << endl;
		
	}
	t4.print();
    cin.get();
    return 0;
}
