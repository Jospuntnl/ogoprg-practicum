﻿#include <iostream>           // nodig voor cout (schrijven naar scherm)
#include <iomanip>            // nodig voor setw (veldbreedte defini�ren)
#include "Tijdsduur.h"

// De declaratie van de ADT Tijdsduur:

ostream& operator<<(ostream& left, const Tijdsduur& right);

ostream& operator<<(ostream& left, const Tijdsduur& right) {
	right.print(left);
	return left;
}

Tijdsduur& Tijdsduur::operator+=(const Tijdsduur& right) {
	this->erbij(right);
	return *this;
}
Tijdsduur operator - (Tijdsduur left, Tijdsduur right) {
	left.eraf(right);
	return left;
}



int main() {
	int i = 0;
	int u = 0;
	int m = 0;
	int min = 0;
	Tijdsduur t4(0, 0);
	Tijdsduur t3(0, 0);
	cout << "Start" << endl;
    Tijdsduur t1(3, 50);        // t1 is 3 uur en 50 minuten
    cout << "t1 = " << t1 << endl;
    const Tijdsduur kw(15);     // kw is 15 minuten
    cout << "kw = " << kw << endl;
    t1 += kw;                   // Tel kw bij t1 op
    cout << "t1 = " << t1 << endl;
    Tijdsduur t2(t1 - kw);      // t2 is t1 min kw
    cout << "t2 = " << t2 << endl;
    cin.get();
    return 0;
}
