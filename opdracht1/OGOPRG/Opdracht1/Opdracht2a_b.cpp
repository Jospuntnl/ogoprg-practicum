#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

class Tijdsduur {
public:

	Tijdsduur(int u, int m) : min(m), uren(u) {
		normaliseer();
	}
	Tijdsduur(int m) : min(m) {
		normaliseer();
	}
	void eraf(Tijdsduur t);
	void print() const;
	Tijdsduur erbij(Tijdsduur t);
	Tijdsduur erbij(int y);
	void maal(int x);
private:
	int uren = 0;
	int min = 0;
	void normaliseer();
};

void Tijdsduur::eraf(Tijdsduur t) {
	min -= t.min;
	normaliseer();
}

Tijdsduur Tijdsduur::erbij(Tijdsduur t) {
	min += t.min;
	normaliseer();
	return uren, min;
}

Tijdsduur Tijdsduur::erbij(int y) {
	min = min + y;
	normaliseer();
	return uren, min;
}

void Tijdsduur::maal(int x) {
	min = (uren * 60) + min;
	min = min * x;
	normaliseer();
}

void Tijdsduur::print() const {
	if(uren == 0){
		cout << min << " minuten.";
	}
	else if ((uren == 1)&&(min == 0)){
		cout << uren << " uur.";
	}
	else if ((uren == 1) && (min >> 0)) {
		cout << uren << " uur en " << min << " minuten.";
	}
	else if ((uren >> 0) && (min == 0)) {
		cout << uren << " uren.";
	}
	else {
		cout << uren << " uren en " << min << " minuten.";
	}
}

void Tijdsduur::normaliseer() {
	if (min >= 60) {
		uren = uren + (min / 60);
		min = (min % 60);
	}
	if (min < 0) {
		uren = uren - 1;
		min = 60 + min;
		normaliseer();
	}
	if (uren < 0) {
		uren = 0;
		min = 0;
	}
}

int main3() {
	Tijdsduur t1(1, 10);
	cout << "t1 = "; t1.print(); cout << endl;
	const Tijdsduur kw(15);
	cout << "kw = "; kw.print(); cout << endl;
	t1.erbij(kw);
	cout << "t1 = "; t1.print(); cout << endl;
	Tijdsduur t2(t1);
	t2.eraf(kw);
	cout << "t2 = "; t2.print(); cout << endl;
	t2.maal(7);
	cout << "t2 = "; t2.print(); cout << endl;
	Tijdsduur t3(3, -122);
	cout << "t3 = "; t3.print(); cout << endl;
	Tijdsduur t4(3, 122);
	cout << "t4 = "; t4.print(); cout << endl;
	Tijdsduur t5(1, -122);
	cout << "t5 = "; t5.print(); cout << endl;
	Tijdsduur t6(1, 10);
	cout << "t6 = "; t6.print(); cout << endl;
	t6.erbij(kw);
	t6.erbij(20);
	cout << "t6 = "; t6.print(); cout << endl;
	cout << "Druk op de return toets." << endl;
	cin.get();
	return 0;

}