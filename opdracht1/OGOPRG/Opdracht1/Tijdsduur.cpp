#include "Tijdsduur.h"
#include <iostream>



void Tijdsduur::eraf(Tijdsduur t) {
	min -= t.min;
}

Tijdsduur Tijdsduur::erbij(Tijdsduur t) {
	min += t.min;
	return  min;
}

Tijdsduur Tijdsduur::erbij(int y) {
	min = min + y;
	return min;
}

void Tijdsduur::maal(int x) {
	min = min * x;
}


void Tijdsduur::print() {
	int uren = 0;
	if (min >= 60) {
		uren = uren + (min / 60);
		min = (min % 60);
	}
	if (min < 0) {
		uren = uren - 1;
		min = 60 + min;
	}
	if (uren < 0) {
		uren = 0;
		min = 0;
	}
	if (uren == 0) {
		cout << min << " minuten.";
	}
	else if ((uren == 1) && (min == 0)) {
		cout << uren << " uur.";
	}
	else if ((uren == 1) && (min >> 0)) {
		cout << uren << " uur en " << min << " minuten.";
	}
	else if ((uren >> 0) && (min == 0)) {
		cout << uren << " uren.";
	}
	else {
		cout << uren << " uren en " << min << " minuten.";
	}
}

void Tijdsduur::print(ostream& o) const {
	// Deze functie moet de Tijdsduur wegschrijven naar de uitvoer genaamd "o"
	int uren = 0;
	int minuten = min;
	if (min >= 60) {
		uren = uren + (minuten / 60);
		minuten = (minuten % 60);
	}
	if (minuten < 0) {
		uren = uren - 1;
		minuten = 60 + minuten;
	}
	if (uren < 0) {
		uren = 0;
		minuten = 0;
	}
	if (uren == 0) {
		o << minuten << " minuten.";
	}
	else if ((uren == 1) && (minuten == 0)) {
		o << uren << " uur.";
	}
	else if ((uren == 1) && (minuten >> 0)) {
		o << uren << " uur en " << minuten << " minuten.";
	}
	else if ((uren >> 0) && (minuten == 0)) {
		o << uren << " uren.";
	}
	else {
		o << uren << " uren en " << minuten << " minuten.";
	}
}

// Deze operator hoef je nu nog niet te begrijpen
// In les 7 wordt dit uitgelegd	
