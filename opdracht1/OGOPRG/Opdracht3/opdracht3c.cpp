#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

// definieer hier de benodigde classes...
class werknemer {
public:
    werknemer(int r) : registratie(r) {

    };
    int geefNummer() const;
    virtual double geefSalaris() const {
        return 0;
    }
private:
    int registratie;
};

class Freelancer : public werknemer {
public:
    Freelancer(int r, double m) : werknemer(r), uurloon(m) {};
    virtual double geefSalaris() const;
    void werkuren(double u);

private:
    double uren;
    double uurloon;
};

class Vastekracht : public werknemer {
public:
    Vastekracht(int r, double m) : werknemer(r), maandloon(m) {};
    virtual double geefSalaris() const;
private:
    double maandloon;
};

class Stukwerker : public werknemer {
public:
    Stukwerker(int r, double s) : werknemer(r), stukprijs(s) {};
    virtual double geefSalaris() const;
    void Produceerstuks(int a);
private:
    int stuks;
    double stukprijs;
};

class Manager : public werknemer {
public:
    Manager(int r) : werknemer(r) {};
    virtual double geefSalaris() const;
    void geeftleidingaan(werknemer);
private:
    double salaris;
    std::vector<werknemer *> ondergeschikten;
};

int werknemer::geefNummer() const {
    return registratie;
}

double Freelancer::geefSalaris() const {
    return (uurloon * uren);
}

void Freelancer::werkuren(double r) {
    uren = r;
}

double Vastekracht::geefSalaris() const {
    return maandloon;
}

double Stukwerker::geefSalaris() const {
    return stuks * stukprijs;
}
void Stukwerker::Produceerstuks(int a) {
    stuks = a;
}

double Manager::geefSalaris() const {
    int l = ondergeschikten.size();
    double totaal = 0;
    for (int i = 0; i < l; i++) {
        totaal = totaal + ondergeschikten[i]->geefSalaris();
    }
    return totaal / l;
}

void Manager::geeftleidingaan(werknemer h) {
    ondergeschikten.push_back(&h);
}

void printMaandSalaris(const werknemer& w) {
    cout << "Werknemer: " << w.geefNummer()
        << " verdient: " << setw(8) << setprecision(2) << fixed
        << w.geefSalaris() << " Euro." << endl;
}

int main() {
    Stukwerker s(1, 1.05);     // werknemer 1 verdient 1.05 per stuk
    Freelancer f(2, 25.75);    // werknemer 2 verdient 25.75 per uur
    Vastekracht v1(3, 1873.53);// werknemer 3 verdient 1873.53 per maand

    Manager m1(4);             // werknemer 4 is de manager van:
    m1.geeftleidingaan(s);     // -  werknemer 1
    m1.geeftleidingaan(f);     // -  werknemer 2
    m1.geeftleidingaan(v1);    // -  werknemer 3

    Vastekracht v2(5, 2036.18);// werknemer 5 verdient 2036,18 per maand
    Manager m2(6);             // werknemer 6 is de manager van:
    m2.geeftleidingaan(v2);    // -  werknemer 5
    m2.geeftleidingaan(m1);    // -  werknemer 4

    s.Produceerstuks(678);     // werknemer 1 produceert 678 stuks
    f.werkuren(84);            // werknemer 2 werkt 84 uren

    printMaandSalaris(s);
    printMaandSalaris(f);
    printMaandSalaris(v1);
    printMaandSalaris(m1);
    printMaandSalaris(v2);
    printMaandSalaris(m2);

    cin.get();
    return 0;
}
