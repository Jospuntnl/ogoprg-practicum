#include <iostream>
#include <iomanip>

using namespace std;

// definieer hier de benodigde classes...
class werknemer {
public:
    werknemer(int r) : registratie(r) {

    };
    int geefNummer() const;
    virtual double geefSalaris() const{
        return 0;
    }
private:
    int registratie;
};

class Freelancer : public werknemer {
public:
    Freelancer(int r, double m) : werknemer(r), uurloon(m) {};
    virtual double geefSalaris() const;
    void werkuren(double u);

private:
    double uren;
    double uurloon;
};

class Vastekracht : public werknemer {
public:
    Vastekracht(int r, double m) : werknemer(r), maandloon(m) {};
    virtual double geefSalaris() const;
private:
    double maandloon;
};

class Stukwerker : public werknemer {
public:
    Stukwerker(int r, double s) : werknemer(r), stukprijs(s) {};
    virtual double geefSalaris() const;
    void Produceerstuks(int a);
private:
    int stuks;
    double stukprijs;
};

int werknemer::geefNummer() const{
    return registratie;
}

double Freelancer::geefSalaris() const {
    return (uurloon * uren);
}

void Freelancer::werkuren(double r) {
    uren = r;
}

double Vastekracht::geefSalaris() const {
    return maandloon;
}

double Stukwerker::geefSalaris() const {
    return stuks * stukprijs;
}
void Stukwerker::Produceerstuks(int a) {
    stuks = a;
}

void printMaandSalaris(const werknemer& w) {
    cout << "Werknemer: " << w.geefNummer()
        << " verdient: " << setw(8) << setprecision(2) << fixed
        << w.geefSalaris() << " Euro." << endl;
}

int main5() {
    Freelancer f(1, 25.75);    // werknemer 1 verdient 25.75 per uur
    Vastekracht v(2, 1873.53); // werknemer 2 verdient 1873.53 per maand
    Stukwerker s(3, 1.05);     // werknemer 3 verdient 1.05 per stuk

    f.werkuren(84);            // werknemer 1 werkt (deze maand) 84 uren
    s.Produceerstuks(1687);    // werknemer 3 produceert (deze maand) 1687 stuks

    cout << "Maand 1:" << endl;
    printMaandSalaris(f);
    printMaandSalaris(v);
    printMaandSalaris(s);

    f.werkuren(13.5);          // werknemer 1 werkt (deze maand) 13.5 uren
    s.Produceerstuks(0);       // werknemer 3 produceert (deze maand) 0 stuks

    cout << "Maand 2:" << endl;
    printMaandSalaris(f);
    printMaandSalaris(v);
    printMaandSalaris(s);

    cin.get();
    return 0;
}
